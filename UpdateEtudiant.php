<?php
require_once("security.php");
require_once("RoleScolarite.php");
?>
<?php
$code=$_POST['code'];
$nom=$_POST['nom'];
$email=$_POST['email'];
$nomPhoto=$_FILES['photo']['name'];
require_once("conn.php");
if($nomPhoto==""){
    $ps=$pdo->prepare("UPDATE  etudiants SET NOM=?,PHOTO=? WHERE CODE=?");
    $param=array($nom,$email,$code);
    $ps->execute($param);
}else{
    $fichierTemp=$_FILES['photo']['tmp_name'];
    move_uploaded_file($fichierTemp,'./images/'.$nomPhoto);
    $ps=$pdo->prepare("UPDATE  etudiants SET NOM=?,EMAIL=?,PHOTO=? WHERE CODE=?");
    $param=array($nom,$email,$nomPhoto,$code);
    $ps->execute($param);
}
header("location:etudiants.php");
?>

